package com.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.ModelAndView;

import com.demo.domain.Account;
import com.demo.domain.Cart;
import com.demo.domain.Comment;
import com.demo.domain.Stock;
import com.demo.domain.User;
import com.demo.service.IAccountService;
import com.demo.service.ICartService;
import com.demo.service.ICommentService;
import com.demo.service.IStockService;
import com.demo.service.IUserService;

@Controller
@SessionAttributes({"item-id"})
public class StockController {

	@Autowired
	private volatile IUserService service;

	@Autowired
	private volatile IAccountService service2;
	
	@Autowired
	private volatile IStockService service3;
	
	@Autowired
	private volatile ICartService service4;
	
	@Autowired
	private volatile ICommentService service6;


	@RequestMapping(value="/stock")
	public ModelAndView createTable(Model model) {
		ModelAndView mv = new ModelAndView("/stock");
		model.addAttribute("searchForm", new Stock());
		List<Stock> stock = service3.readAll();
		mv.addObject("stockList", stock);
		Object u = RequestContextHolder.currentRequestAttributes().getAttribute("user-id", RequestAttributes.SCOPE_SESSION);
		String userid = u.toString();
		Long id = Long.valueOf(userid);
		List<Cart> cart = service4.readAllInCartNotBought(id);
		if(cart != null){
			mv.addObject("cart", cart.size());
		}
		return mv;
	}
	
	
	@RequestMapping(value="/stock-info/{id}", method= RequestMethod.GET)
	public ModelAndView createStockInfo(@PathVariable("id")Long id , Model model) {
		Stock stock = service3.readOne(id);
		ModelAndView mv = new ModelAndView("/stock-info"); 
		model.addAttribute("cartForm", new Cart());
		mv.addObject("stockItem", stock);
		model.addAttribute("item-id", stock.getId());
		//ArrayList<Integer> choice = quantityChoice();
		ArrayList<Integer> choice = new ArrayList<Integer>();
		int count = 1;
		for(int i = 0; i < stock.getQuantity(); i++){
			choice.add(count);
			count++;
		}
		mv.addObject("quantityList", choice);
		List<Comment> comment = service6.allByStock(id);
		mv.addObject("comment", comment);
		Object u = RequestContextHolder.currentRequestAttributes().getAttribute("user-id", RequestAttributes.SCOPE_SESSION);
		String userid = u.toString();
		Long id2 = Long.valueOf(userid);
		List<Cart> cart = service4.readAllInCartNotBought(id2);
		if(cart != null){
			mv.addObject("cart", cart.size());
		}
		
	
		return mv;
		
	}
	
	@RequestMapping(value="/stock-info/added-to-cart", method=RequestMethod.POST)
	public ModelAndView addToCart(@ModelAttribute Cart cart, Model model){
		model.addAttribute("cartForm", cart);
		Object u = RequestContextHolder.currentRequestAttributes().getAttribute("user-id", RequestAttributes.SCOPE_SESSION);
		String userid = u.toString();
		Long id = Long.valueOf(userid);
		User us = service.read(id);
		
		Object i = RequestContextHolder.currentRequestAttributes().getAttribute("item-id", RequestAttributes.SCOPE_SESSION);
		String itemid = i.toString();
		Long iid = Long.valueOf(itemid);
		Stock stock = service3.readOne(iid);
		
		if(cart.getQuantity() > stock.getQuantity()){
			ModelAndView mv = new ModelAndView("/error"); 
			return mv;
		}else{
		
		cart.setBuyer(us);
		cart.setCartItem(stock);
		cart.setPrice(stock.getPrice() * cart.getQuantity());
		service4.create(cart);
		
		ModelAndView mv = new ModelAndView("/added-to-cart"); 
		Object u2 = RequestContextHolder.currentRequestAttributes().getAttribute("user-id", RequestAttributes.SCOPE_SESSION);
		String user2id = u2.toString();
		Long id2 = Long.valueOf(user2id);
		List<Cart> cart2 = service4.readAllInCartNotBought(id2);
		if(cart2 != null){
			mv.addObject("cart", cart2.size());
		}
		return mv;
		}
	}
	
	
	
	private ArrayList<Integer> quantityChoice() {
		ArrayList<Integer> choice = new ArrayList<Integer>();
		choice.add(1);
		choice.add(2);
		choice.add(3);
		choice.add(4);
		choice.add(5);
		choice.add(6);
		choice.add(7);
		choice.add(8);
		choice.add(9);
		choice.add(10);
		return choice;
	}
}
