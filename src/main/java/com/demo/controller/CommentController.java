package com.demo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.ModelAndView;

import com.demo.domain.Cart;
import com.demo.domain.Comment;
import com.demo.domain.PurchaseHistory;
import com.demo.domain.Stock;
import com.demo.domain.User;
import com.demo.service.IAccountService;
import com.demo.service.ICartService;
import com.demo.service.ICommentService;
import com.demo.service.IHistoryService;
import com.demo.service.IStockService;
import com.demo.service.IUserService;

@Controller
@SessionAttributes({"review-id" , "history-id"})
public class CommentController {

	@Autowired
	private volatile IUserService service;

	@Autowired
	private volatile IAccountService service2;

	@Autowired
	private volatile IStockService service3;

	@Autowired
	private volatile ICartService service4;

	@Autowired
	private volatile IHistoryService service5;

	@Autowired
	private volatile ICommentService service6;


	@RequestMapping(value="/write-review/{id}", method=RequestMethod.GET)
	public ModelAndView viewReview(@PathVariable("id")Long id , Model model) { 
		model.addAttribute("reviewForm", new Comment());
		PurchaseHistory ph = service5.getOne(id);
		model.addAttribute("history-id", ph.getId());
		model.addAttribute("review-id", ph.getHistory1().getId());
		ModelAndView mv = new ModelAndView("/write-review"); 
		ArrayList<Integer> choice = rating();
		mv.addObject("rating", choice);
		Object uu = RequestContextHolder.currentRequestAttributes().getAttribute("user-id", RequestAttributes.SCOPE_SESSION);
		String user2id = uu.toString();
		Long id2 = Long.valueOf(user2id);
		List<Cart> cart2 = service4.readAllInCartNotBought(id2);
		if(cart2 != null){
			mv.addObject("cart", cart2.size());
		}
		return mv;
	}

	@RequestMapping(value="/write-review/review-completed", method=RequestMethod.POST)
	public ModelAndView processReview(@ModelAttribute Comment comment, Model model) {
		model.addAttribute("reviewForm", comment);
		Date date = new Date();
		//product id
		Object product = RequestContextHolder.currentRequestAttributes().getAttribute("review-id", RequestAttributes.SCOPE_SESSION);
		String productid = product.toString();
		Long pid = Long.valueOf(productid);
		Stock s = service3.readOne(pid);

		//user id
		Object u = RequestContextHolder.currentRequestAttributes().getAttribute("user-id", RequestAttributes.SCOPE_SESSION);
		String userid = u.toString();
		Long id = Long.valueOf(userid);
		User user = service.read(id);

		//get history
		Object h = RequestContextHolder.currentRequestAttributes().getAttribute("history-id", RequestAttributes.SCOPE_SESSION);
		String historyid = h.toString();
		Long hid = Long.valueOf(historyid);
		PurchaseHistory ph = service5.getOne(hid);
		ph.setReviewed(true);
		service5.create(ph);

		comment.setCommentStock(s);
		comment.setCommentUser(user);
		comment.setDate(date);
		service6.create(comment);



		ModelAndView mv = new ModelAndView("/user-mainpage"); 
		Object uu = RequestContextHolder.currentRequestAttributes().getAttribute("user-id", RequestAttributes.SCOPE_SESSION);
		String user2id = uu.toString();
		Long id2 = Long.valueOf(user2id);
		List<Cart> cart2 = service4.readAllInCartNotBought(id2);
		if(cart2 != null){
			mv.addObject("cart", cart2.size());
		}
		return mv;
	}


	@RequestMapping(value="/my-review/{id}", method= RequestMethod.GET)
	public ModelAndView viewMyReview(@PathVariable("id")Long id , Model model) {
		Object u = RequestContextHolder.currentRequestAttributes().getAttribute("user-id", RequestAttributes.SCOPE_SESSION);
		String userid = u.toString();
		Long uid = Long.valueOf(userid);
		Comment comment = service6.findMyComment(id, uid);
		ModelAndView mv = new ModelAndView("/my-review"); 
		mv.addObject("comment", comment);
		Object uu = RequestContextHolder.currentRequestAttributes().getAttribute("user-id", RequestAttributes.SCOPE_SESSION);
		String user2id = uu.toString();
		Long id2 = Long.valueOf(user2id);
		List<Cart> cart2 = service4.readAllInCartNotBought(id2);
		if(cart2 != null){
			mv.addObject("cart", cart2.size());
		}
		return mv;


	}




	private ArrayList<Integer> rating() {
		ArrayList<Integer> choice = new ArrayList<Integer>();
		choice.add(1);
		choice.add(2);
		choice.add(3);
		choice.add(4);
		choice.add(5);
		return choice;
	}
}
