package com.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.ModelAndView;

import com.demo.domain.Cart;
import com.demo.domain.PurchaseHistory;
import com.demo.service.IAccountService;
import com.demo.service.ICartService;
import com.demo.service.IHistoryService;
import com.demo.service.IStockService;
import com.demo.service.IUserService;

@Controller
@SessionAttributes({})
public class HistoryController {

	@Autowired
	private volatile IUserService service;

	@Autowired
	private volatile IAccountService service2;

	@Autowired
	private volatile IStockService service3;

	@Autowired
	private volatile ICartService service4;
	
	@Autowired
	private volatile IHistoryService service5;

	
	
	@RequestMapping(value="/my-history")
	public ModelAndView createCheckout(Model model) {
		ModelAndView mv = new ModelAndView("/my-history");
		Object u = RequestContextHolder.currentRequestAttributes().getAttribute("user-id", RequestAttributes.SCOPE_SESSION);
		String userid = u.toString();
		Long id = Long.valueOf(userid);
		List<PurchaseHistory> history = service5.allByUser(id);
		int count = 0;
		for(int i = 0; i < history.size(); i++){
			history.get(count).getHistory1().setPrice(history.get(count).getHistory1().getPrice() * history.get(count).getQuantity() );
		count++;
		}
		mv.addObject("history", history);
		Object uu = RequestContextHolder.currentRequestAttributes().getAttribute("user-id", RequestAttributes.SCOPE_SESSION);
		String user2id = uu.toString();
		Long id2 = Long.valueOf(user2id);
		List<Cart> cart2 = service4.readAllInCartNotBought(id2);
		if(cart2 != null){
			mv.addObject("cart", cart2.size());
		}
		return mv;
	}
}
