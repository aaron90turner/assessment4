package com.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.swing.text.View;
import javax.validation.Valid;

import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;

import com.demo.domain.Account;
import com.demo.domain.Cart;
import com.demo.domain.User;
import com.demo.service.IAccountService;
import com.demo.service.ICartService;
import com.demo.service.IUserService;
import com.demo.service.UserService;





@Controller
@SessionAttributes({"name", "user-name" , "user-id"})
public class UserController {

	@Autowired
	private volatile IUserService service;

	@Autowired
	private volatile IAccountService service2;
	
	@Autowired
	private volatile ICartService service4;

	public static boolean loggedIn = false;
	public static boolean adminloggedIn = false;



	@RequestMapping(value="/register", method=RequestMethod.GET)
	public ModelAndView viewRegistration(Model model) { 
		model.addAttribute("userForm", new User());
		return new ModelAndView("register");
	}

	@RequestMapping(value="/registered", method=RequestMethod.POST)
	public ModelAndView processRegistration(@ModelAttribute("userForm") @Valid  User user, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			return new ModelAndView("register");
		}

		model.addAttribute("userForm", user);
		service.create(user);


		return new ModelAndView("register-complete");
	}


	@RequestMapping(value="/login", method=RequestMethod.GET)
	public ModelAndView viewLogin(Model model) { 
		model.addAttribute("userForm", new User());
		return new ModelAndView("login");
	}

	@RequestMapping(value="/login-complete", method=RequestMethod.POST)
	public ModelAndView processLogin(@ModelAttribute User user, Model model) {
		model.addAttribute("userForm", user);
		String username = user.getUsername();
		String password = user.getPassword();
		User user2 = service.login(username, password);

		if(user2 == null){
			return new ModelAndView("login-error");
		}else if(user2.getUsername().contains("admin") && user2.getPassword().contains("admin123")){
			adminloggedIn = true;
			model.addAttribute("name", user2.getName() );
			model.addAttribute("user-name", user2.getUsername() );

			return new ModelAndView("admin-mainpage");
		}

		else{

			model.addAttribute("name", user2.getName() );
			model.addAttribute("user-name", user2.getUsername() );
			model.addAttribute("user-id", user2.getId() );
			loggedIn = true;
			Long id = user2.getId();
			Account a = service2.getAccount(id);
			if(a == null){
				return new ModelAndView("account-error");
			}else{
				return new ModelAndView("user-mainpage");
			}
		}





	}






	@RequestMapping(value="/user-mainpage")
	public ModelAndView Main() {
		ModelAndView mv = new ModelAndView("/user-mainpage");
		Object uu = RequestContextHolder.currentRequestAttributes().getAttribute("user-id", RequestAttributes.SCOPE_SESSION);
		String user2id = uu.toString();
		Long id2 = Long.valueOf(user2id);
		List<Cart> cart2 = service4.readAllInCartNotBought(id2);
		if(cart2 != null){
			mv.addObject("cart", cart2.size());
		}
		return mv;
	}
	
	@RequestMapping(value="/admin-mainpage")
	public ModelAndView Admin() {
		return new ModelAndView("admin-mainpage");
	}

	@RequestMapping(value="/login-error")
	public ModelAndView LoginErrorPage() {
		return new ModelAndView("login-error");
	}
	
	@RequestMapping(value="/test")
	public ModelAndView TestPage() {
		return new ModelAndView("test");
	}


	@RequestMapping(value="/logout")
	public ModelAndView LogoutPage(Model model , WebRequest request , SessionStatus status) {
		loggedIn = false;
		adminloggedIn = false;
		status.setComplete();
		request.removeAttribute("name", WebRequest.SCOPE_SESSION);
		request.removeAttribute("user-id", WebRequest.SCOPE_SESSION);
		request.removeAttribute("user-name", WebRequest.SCOPE_SESSION);
		return new ModelAndView("main");
	}

}

