package com.demo.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.ModelAndView;

import com.demo.domain.Account;
import com.demo.domain.Card;
import com.demo.domain.Cart;
import com.demo.domain.Comment;
import com.demo.domain.PurchaseHistory;
import com.demo.domain.Stock;
import com.demo.domain.User;
import com.demo.service.IAccountService;
import com.demo.service.ICartService;
import com.demo.service.ICommentService;
import com.demo.service.IHistoryService;
import com.demo.service.IStockService;
import com.demo.service.IUserService;

@Controller
@SessionAttributes({"user-account" , "stock-reviewed" , "updated" , "his-name"})
public class AdminController {

	@Autowired
	private volatile IUserService service;

	@Autowired
	private volatile IAccountService service2;

	@Autowired
	private volatile IStockService service3;

	@Autowired
	private volatile ICartService service4;

	@Autowired
	private volatile IHistoryService service5;
	
	@Autowired
	private volatile ICommentService service6;
	
	@Autowired
	ServletContext servletContext;


	@RequestMapping(value="/all-history")
	public ModelAndView allHistory(Model model) {
		ModelAndView mv = new ModelAndView("/all-history");
		List<PurchaseHistory> history = service5.readAll();
		int count = 0;
		for(int i = 0; i < history.size(); i++){
			history.get(count).getHistory1().setPrice(history.get(count).getHistory1().getPrice() * history.get(count).getQuantity() );
			count++;
		}
		mv.addObject("history", history);
		return mv;
	}
	@RequestMapping(value="/all-users")
	public ModelAndView allUsers(Model model) {
		ModelAndView mv = new ModelAndView("/all-users");
		List<User> account = service.readAll();
		List<User> account2 = new ArrayList<User>();
		int count = 0;
		for(int i = 0; i < account.size(); i++){
			if(!account.get(count).getUsername().contains("admin")){
				account2.add(account.get(count));
			}
				count++;
		}
		mv.addObject("account", account2);
		return mv;

	}

	@RequestMapping(value="/account-info/{id}", method= RequestMethod.GET)
	public ModelAndView accountInfo(@PathVariable("id")Long id , Model model) {
		ModelAndView mv = new ModelAndView("/account-info"); 
		Account account = service2.getAccount(id);
		User u = service.read(id);
		model.addAttribute("user-account", u.getName() );

		mv.addObject("account", account);

		return mv;

	}

	@RequestMapping(value="/add-stock", method=RequestMethod.GET)
	public ModelAndView viewAddStock(Model model) { 
		model.addAttribute("stockForm", new Stock());
		ModelAndView mv = new ModelAndView("/add-stock"); 
		return mv;
	}

	@RequestMapping(value="/stock-added", method=RequestMethod.POST)
	public ModelAndView addStock(@ModelAttribute("stockForm") @Valid  Stock stock, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			return new ModelAndView("add-stock");
		}
		 FileOutputStream outputStream = null;
		 		 String webappRoot = servletContext.getRealPath("/");
		         String filePath = webappRoot + "resources"
		                  + File.separator + "img" + File.separator
		                  + stock.getFile().getOriginalFilename();
		          try {
		              outputStream = new FileOutputStream(new File(filePath));
		              outputStream.write(stock.getFile().getFileItem().get());
		              outputStream.close();
		              System.out.println("file saved");
		              System.out.println("file path " + filePath);
		              System.out.println("file name " + stock.getFile().getOriginalFilename());
		              System.out.println("file name2 " + stock.getImage());
		              stock.setFile(null);
		          } catch (Exception e) {
		              System.out.println("Error while saving file");
		              
		          }
		          
		          service3.create(stock);
		
		return new ModelAndView("stock-added");
	}

	@RequestMapping(value="/view-stock")
	public ModelAndView viewStock(Model model) {
		ModelAndView mv = new ModelAndView("/view-stock");
		List<Stock> stock = service3.readAll();
		mv.addObject("stockList", stock);

		return mv;
	}

	@RequestMapping(value="/update-stock/{id}", method= RequestMethod.GET)
	public ModelAndView updateStock(@PathVariable("id")Long id , Model model) {
		model.addAttribute("updateForm", new Stock());
		ModelAndView mv = new ModelAndView("/update-stock");
		Stock stock = service3.readOne(id);
		mv.addObject("stock", stock);
		model.addAttribute("updated", stock.getId());
		return mv;


	}
	
	@RequestMapping(value="/update-stock/stock-updated", method=RequestMethod.POST)
	public ModelAndView updateStock(@ModelAttribute Stock stock, Model model) {
		model.addAttribute("updateForm", stock);
		Object u = RequestContextHolder.currentRequestAttributes().getAttribute("updated", RequestAttributes.SCOPE_SESSION);
		String sid = u.toString();
		Long id = Long.valueOf(sid);
		Stock stock2 = service3.readOne(id);
		stock2.setImage(stock2.getImage());
		stock2.setTitle(stock.getTitle());
		stock2.setManufacturer(stock.getManufacturer());
		stock2.setCatagory(stock.getCatagory());
		stock2.setQuantity(stock.getQuantity());
		stock2.setPrice(stock.getPrice());
		
		service3.create(stock2);
		return new ModelAndView("stock-updated");
	}
	
	@RequestMapping(value="/stock-reviews/{id}", method= RequestMethod.GET)
	public ModelAndView createStockReview(@PathVariable("id")Long id , Model model) {
		ModelAndView mv = new ModelAndView("/stock-reviews"); 
		Stock s = service3.readOne(id);
		List<Comment> comment = service6.allByStock(id);
		mv.addObject("comment", comment);
		model.addAttribute("stock-reviewed", s.getTitle());
		return mv;
		
	}
	
	@RequestMapping(value="/user-history/{id}", method= RequestMethod.GET)
	public ModelAndView userHistory(@PathVariable("id")Long id , Model model) {
		ModelAndView mv = new ModelAndView("/user-history"); 
		List<PurchaseHistory> history = service5.allByUser(id);
		User u = service.read(id);
		int count = 0;
		for(int i = 0; i < history.size(); i++){
			history.get(count).getHistory1().setPrice(history.get(count).getHistory1().getPrice() * history.get(count).getQuantity() );
		count++;
		}
		mv.addObject("history", history);
		model.addAttribute("his-name", u.getUsername());
		return mv;
		
	}

}
