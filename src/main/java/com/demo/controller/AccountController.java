package com.demo.controller;

import java.util.ArrayList;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.ModelAndView;

import com.demo.domain.Account;
import com.demo.domain.Cart;
import com.demo.domain.Stock;
import com.demo.domain.User;
import com.demo.service.IAccountService;
import com.demo.service.ICartService;
import com.demo.service.IStockService;
import com.demo.service.IUserService;

@Controller
@SessionAttributes({""})
public class AccountController {

	@Autowired
	private volatile IUserService service;

	@Autowired
	private volatile IAccountService service2;
	
	@Autowired
	private volatile IStockService service3;

	@Autowired
	private volatile ICartService service4;

	@RequestMapping(value="/create-account", method=RequestMethod.GET)
	public ModelAndView viewRegistration(Model model) { 
		model.addAttribute("accountForm", new Account());
		ModelAndView mv = new ModelAndView("/create-account"); 
		ArrayList<String> choice = cardChoice();
		mv.addObject("cardList", choice);
		return mv;
	}

	

	@RequestMapping(value="/account-created", method=RequestMethod.POST)
	public ModelAndView processRegistration(@ModelAttribute("accountForm") @Valid  Account account, BindingResult bindingResult, Model model) {
		if (bindingResult.hasErrors()) {
			return new ModelAndView("create-account");
		}

		Object u = RequestContextHolder.currentRequestAttributes().getAttribute("user-id", RequestAttributes.SCOPE_SESSION);
		String userid = u.toString();
		Long id = Long.valueOf(userid);
		User us = service.read(id);
		account.setUser(us);
		service2.create(account);


		return new ModelAndView("user-mainpage");
	}
	
	
	private ArrayList<String> cardChoice() {
		ArrayList<String> choice = new ArrayList<String>();
		choice.add("Visa");
		choice.add("MasterCard");
		choice.add("Diners");
		return choice;
	}
}
