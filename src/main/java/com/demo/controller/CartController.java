package com.demo.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.ModelAndView;

import com.demo.card.CreditCard;
import com.demo.card.DinersCard;
import com.demo.card.MasterCard;
import com.demo.card.VisaCard;
import com.demo.domain.Account;
import com.demo.domain.Card;
import com.demo.domain.Cart;
import com.demo.domain.PurchaseHistory;
import com.demo.domain.Stock;
import com.demo.domain.User;
import com.demo.service.IAccountService;
import com.demo.service.ICartService;
import com.demo.service.IHistoryService;
import com.demo.service.IStockService;
import com.demo.service.IUserService;

@Controller
@SessionAttributes({"error"})
public class CartController {

	@Autowired
	private volatile IUserService service;

	@Autowired
	private volatile IAccountService service2;

	@Autowired
	private volatile IStockService service3;

	@Autowired
	private volatile ICartService service4;
	
	@Autowired
	private volatile IHistoryService service5;




	@RequestMapping(value="/checkout")
	public ModelAndView createCheckout(Model model) {
		ModelAndView mv = new ModelAndView("/checkout");
		Object u = RequestContextHolder.currentRequestAttributes().getAttribute("user-id", RequestAttributes.SCOPE_SESSION);
		String userid = u.toString();
		Long id = Long.valueOf(userid);
		List<Cart> cart = service4.readAllInCartNotBought(id);
		mv.addObject("cart", cart);
		Object uu = RequestContextHolder.currentRequestAttributes().getAttribute("user-id", RequestAttributes.SCOPE_SESSION);
		String user2id = uu.toString();
		Long id2 = Long.valueOf(user2id);
		List<Cart> cart2 = service4.readAllInCartNotBought(id2);
		if(cart2 != null){
			mv.addObject("cart2", cart2.size());
		}
		return mv;
	}
	
	@RequestMapping(value="/empty")
	public ModelAndView emptyCart(Model model) {
		ModelAndView mv = new ModelAndView("/cart-empty");
		Object u = RequestContextHolder.currentRequestAttributes().getAttribute("user-id", RequestAttributes.SCOPE_SESSION);
		String userid = u.toString();
		Long id = Long.valueOf(userid);
		List<Cart> cart = service4.readAllInCartNotBought(id);
		int count = 0;
		for(int i = 0; i < cart.size(); i++){
			service4.delete(cart.get(count).getId());
			count++;
		}
		Object uu = RequestContextHolder.currentRequestAttributes().getAttribute("user-id", RequestAttributes.SCOPE_SESSION);
		String user2id = uu.toString();
		Long id2 = Long.valueOf(user2id);
		List<Cart> cart2 = service4.readAllInCartNotBought(id2);
		if(cart2 != null){
			mv.addObject("cart", cart2.size());
		}
		return mv;
	}

	@RequestMapping(value="/checkout/personal-info")
	public ModelAndView createPersonalInfo(Model model) {
		Object u = RequestContextHolder.currentRequestAttributes().getAttribute("user-id", RequestAttributes.SCOPE_SESSION);
		String userid = u.toString();
		Long id = Long.valueOf(userid);
		User us = service.read(id);
		Account account = service2.getAccount(id);
		ModelAndView mv = new ModelAndView("/personal-info");
		mv.addObject("account", account);
		mv.addObject("user", us);
		Object uu = RequestContextHolder.currentRequestAttributes().getAttribute("user-id", RequestAttributes.SCOPE_SESSION);
		String user2id = uu.toString();
		Long id2 = Long.valueOf(user2id);
		List<Cart> cart2 = service4.readAllInCartNotBought(id2);
		if(cart2 != null){
			mv.addObject("cart", cart2.size());
		}
		return mv;
	}

	@RequestMapping(value="/checkout/personal-info/payment-info", method=RequestMethod.GET)
	public ModelAndView viewPaymentInfo(Model model) { 
		model.addAttribute("cardForm", new Card());
		ModelAndView mv = new ModelAndView("/payment-info");
		Object uu = RequestContextHolder.currentRequestAttributes().getAttribute("user-id", RequestAttributes.SCOPE_SESSION);
		String user2id = uu.toString();
		Long id2 = Long.valueOf(user2id);
		List<Cart> cart2 = service4.readAllInCartNotBought(id2);
		if(cart2 != null){
			mv.addObject("cart", cart2.size());
		}
        return mv;
	}

	@RequestMapping(value="/checkout/personal-info/process", method=RequestMethod.POST)
	public ModelAndView processPayment(@ModelAttribute Card card, Model model) {
		model.addAttribute("cardForm", card);
		Object u = RequestContextHolder.currentRequestAttributes().getAttribute("user-id", RequestAttributes.SCOPE_SESSION);
		String userid = u.toString();
		Long id = Long.valueOf(userid);
		Account account = service2.getAccount(id);
		CreditCard creditCard = null;
		if (account.getPaymentMethod().contains("Diners")){
			creditCard = new DinersCard(card.getCardNumber(),card.getExpMonth(), card.getExpYear());
		}
		if (account.getPaymentMethod().contains("Visa")){
			creditCard = new VisaCard(card.getCardNumber(),card.getExpMonth(), card.getExpYear());
		}
		if (account.getPaymentMethod().contains("MasterCard")){
			creditCard = new MasterCard(card.getCardNumber(),card.getExpMonth(), card.getExpYear());

		}

		if (creditCard != null && creditCard.isValid()){
			System.out.println("success");
			model.addAttribute("error", "");
			return createProcess(model);
		}else{
			System.out.println("error");
			model.addAttribute("error", "invalid CreditCard Details");
			return viewPaymentInfo( model);
		}




	}

	@RequestMapping(value="/checkout/personal-info/process")
	public ModelAndView createProcess(Model model) {
		ModelAndView mv = new ModelAndView("/process-payment");
		Object u = RequestContextHolder.currentRequestAttributes().getAttribute("user-id", RequestAttributes.SCOPE_SESSION);
		String userid = u.toString();
		Long id = Long.valueOf(userid);
		List<Cart> cart = service4.readAllInCartNotBought(id);
		int count = 0;
		double total = 0;
		double discount = 0;
		double ntotal = 0;
		for(int i = 0; i < cart.size(); i++){
			total = total + cart.get(count).getPrice();
			count++;
		}
		List<PurchaseHistory> ph = service5.allByUser(id);
		if(ph != null){
			discount = total * 0.10;
			ntotal = total - discount;
			mv.addObject("discount", discount);
			mv.addObject("ntotal", ntotal);
		}
		mv.addObject("total", total);
		Object uu = RequestContextHolder.currentRequestAttributes().getAttribute("user-id", RequestAttributes.SCOPE_SESSION);
		String user2id = uu.toString();
		Long id2 = Long.valueOf(user2id);
		List<Cart> cart2 = service4.readAllInCartNotBought(id2);
		if(cart2 != null){
			mv.addObject("cart", cart2.size());
		}

		return mv;
	}
	
	@RequestMapping(value="/completed-payment")
	public ModelAndView completePayment(Model model) {
		ModelAndView mv = new ModelAndView("/completed-payment");
		Object u = RequestContextHolder.currentRequestAttributes().getAttribute("user-id", RequestAttributes.SCOPE_SESSION);
		String userid = u.toString();
		Long id = Long.valueOf(userid);
		List<Cart> cart = service4.readAllInCartNotBought(id);
		int count = 0;
		Date d = new Date();
		for(int i = 0; i < cart.size(); i++){
			cart.get(count).setBought(true);
			Stock s = cart.get(count).getCartItem();
			s.setQuantity(s.getQuantity() - cart.get(count).getQuantity());
			service3.create(s);
			service4.create(cart.get(count));
			PurchaseHistory history = new PurchaseHistory();
			history.setDate(d);
			history.setHistory(cart.get(count).getBuyer());
			history.setHistory1(cart.get(count).getCartItem());
			history.setQuantity(cart.get(count).getQuantity());
			service5.create(history);
			
			count++;
		}
		Object uu = RequestContextHolder.currentRequestAttributes().getAttribute("user-id", RequestAttributes.SCOPE_SESSION);
		String user2id = uu.toString();
		Long id2 = Long.valueOf(user2id);
		List<Cart> cart2 = service4.readAllInCartNotBought(id2);
		if(cart2 != null){
			mv.addObject("cart", cart2.size());
		}
		
		return mv;
	}
}
