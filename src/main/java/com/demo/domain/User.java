package com.demo.domain;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;



@Entity
public class User {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@NotEmpty(message="Name: Please enter name")
	private String name;

	@NotEmpty(message="Username: Please enter username")
	private String username;

	@NotEmpty(message="Email: Please enter email")
	private String email;

	@NotEmpty(message="Password: Please enter password")
	private String password;

	@OneToMany(mappedBy="history", cascade=CascadeType.ALL)
	  private List<PurchaseHistory> his = new ArrayList<PurchaseHistory>();
	
	@OneToMany(mappedBy="buyer", cascade=CascadeType.ALL)
	  private List<Cart> buy = new ArrayList<Cart>();


	@OneToMany(mappedBy="commentUser", cascade=CascadeType.ALL)
	  private List<Comment> comment = new ArrayList<Comment>();








	

	public List<Comment> getComment() {
		return comment;
	}

	public void setComment(List<Comment> comment) {
		this.comment = comment;
	}

	public List<Cart> getBuy() {
		return buy;
	}

	public void setBuy(List<Cart> buy) {
		this.buy = buy;
	}

	public List<PurchaseHistory> getHis() {
		return his;
	}

	public void setHis(List<PurchaseHistory> his) {
		this.his = his;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {

		return "User [name: " + name + " username: " + username + " email: " + email + "]";

	}


}


