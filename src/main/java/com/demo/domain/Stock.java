package com.demo.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

@Entity
public class Stock {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;

	@NotEmpty(message="Title: Please enter Title")
	private String title;
	
	@NotEmpty(message="Manufacturer: Please enter Manufacturer")
	private String manufacturer;
	
	private double price;
	
	@NotEmpty(message="Catagory: Please enter Catagory")
	private String catagory;
	
	private String image = null;
	
	private int quantity;
	
	@OneToMany(mappedBy="history1", cascade=CascadeType.ALL)
	  private List<PurchaseHistory> his1 = new ArrayList<PurchaseHistory>();
	
	@OneToMany(mappedBy="cartItem", cascade=CascadeType.ALL)
	  private List<Cart> cart = new ArrayList<Cart>();
	
	@OneToMany(mappedBy="commentStock", cascade=CascadeType.ALL)
	  private List<Comment> comment = new ArrayList<Comment>();
	
	
	private CommonsMultipartFile file = null;


	public CommonsMultipartFile getFile() {
		return file;
	}
	public void setFile(CommonsMultipartFile file) {
		this.file = file;
		this.image = file.getOriginalFilename();
	}
   

	public List<Comment> getComment() {
		return comment;
	}
	public void setComment(List<Comment> comment) {
		this.comment = comment;
	}
	public List<Cart> getCart() {
		return cart;
	}
	public void setCart(List<Cart> cart) {
		this.cart = cart;
	}
	public List<PurchaseHistory> getHis1() {
		return his1;
	}
	public void setHis1(List<PurchaseHistory> his1) {
		this.his1 = his1;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	
	
	
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getCatagory() {
		return catagory;
	}
	public void setCatagory(String catagory) {
		this.catagory = catagory;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	



}
