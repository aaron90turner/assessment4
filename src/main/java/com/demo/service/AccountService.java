package com.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.demo.domain.Account;
import com.demo.repository.jpa.IAccountRepository;
import com.demo.repository.jpa.IUserRepository;

@Service
@Transactional
public class AccountService implements IAccountService {
	
	@Autowired
	private IAccountRepository repository;

	@Override
	public Account create(Account a) {
		
		return repository.save(a);
	}

	@Override
	public Account getAccount(Long id) {
		
		Account a = repository.findByUser_id(id);
		return a;
	}

	@Override
	public List<Account> readAll() {
		List<Account> account = repository.findAll();		
		return account;
	}

}
