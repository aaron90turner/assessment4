package com.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.demo.domain.Cart;
import com.demo.repository.jpa.ICartRepository;
import com.demo.repository.jpa.IStockRepository;

@Service
@Transactional
public class CartService implements ICartService {

	@Autowired
	private ICartRepository repository;

	@Override
	public Cart create(Cart cart) {
		return repository.save(cart);
	}

	@Override
	public List<Cart> readAllInCart(Long id) {
		List<Cart> c = repository.findAllByBuyer_id(id);
		return c;
	}

	@Override
	public List<Cart> readAllInCartNotBought(Long id) {
		List<Cart> c = repository.findAllByBuyer_idAndBoughtFalse(id);
		return c;
	}

	@Override
	public Cart delete(Long id) {
		repository.delete(id);
		return null;
	}
}
