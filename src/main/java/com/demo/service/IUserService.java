package com.demo.service;

import java.util.List;

import com.demo.domain.User;

public interface IUserService {
	
	public User create(User user);
	 
	public User read(Long id);
 
	public List<User> readAll();
 
	public User update(User user);
 
	public User delete(Long id);
	
	public User login(String username , String password);
	
	

}
