package com.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.demo.domain.PurchaseHistory;
import com.demo.repository.jpa.IAccountRepository;
import com.demo.repository.jpa.IHistoryRepository;

@Service
@Transactional
public class HistoryService implements IHistoryService {
	
	@Autowired
	private IHistoryRepository repository;

	@Override
	public PurchaseHistory create(PurchaseHistory p) {
		
		return repository.save(p);
	}

	@Override
	public List<PurchaseHistory> readAll() {
		
		return repository.findAll();
	}

	@Override
	public List<PurchaseHistory> allByUser(Long id) {
		
		return repository.findAllByHistory_id(id);
	}

	@Override
	public PurchaseHistory getOne(Long id) {
		PurchaseHistory ph = repository.findOne(id);
		return ph;
	}

}
