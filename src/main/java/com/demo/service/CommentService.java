package com.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.demo.domain.Comment;
import com.demo.repository.jpa.ICommentRepository;

@Service
@Transactional
public class CommentService implements ICommentService {

	@Autowired
	private ICommentRepository repository;
	
	@Override
	public Comment create(Comment c) {
		
		return repository.save(c);
	}

	@Override
	public List<Comment> allByUser(Long id) {
		List<Comment> c = repository.findAllByCommentUser_id(id);
		return c;
	}

	@Override
	public List<Comment> allByStock(Long id) {
		List<Comment> c = repository.findAllByCommentStock_id(id);
		return c;
	}

	@Override
	public Comment findMyComment(Long id, Long id2) {
		Comment c = repository.findByCommentStock_idAndCommentUser_id(id, id2);
		return c;
	}

}
