package com.demo.service;

import java.util.List;

import com.demo.domain.PurchaseHistory;

public interface IHistoryService {
	
	public PurchaseHistory create(PurchaseHistory p);
	
	public List<PurchaseHistory> readAll();
	
	public List<PurchaseHistory> allByUser(Long id);
	
	public PurchaseHistory getOne(Long id);

}
