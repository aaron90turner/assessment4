package com.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.demo.domain.Stock;
import com.demo.domain.User;
import com.demo.repository.jpa.IStockRepository;
import com.demo.repository.jpa.IUserRepository;

@Service
@Transactional
public class StockService implements IStockService {

	@Autowired
	private IStockRepository repository;
	
	@Override
	public Stock create(Stock s) {
		
		return repository.save(s);
	}

	public Stock update(Stock s) {
		Stock existingUser = repository.findOne(s.getId());

		// Assign new values
		existingUser.setTitle(s.getTitle());
		existingUser.setManufacturer(s.getManufacturer());
		existingUser.setCatagory(s.getCatagory());
		existingUser.setQuantity(s.getQuantity());
		existingUser.setPrice(s.getPrice());

		return repository.save(existingUser);
	}

	@Override
	public Stock delete(Long id) {
		repository.delete(id);
		return null;
	}

	@Override
	public List<Stock> readCatagory(String cata) {
		
		
		List<Stock> s = repository.findAll();
		List<Stock> ss = new ArrayList<Stock>();
		int count = 0;
		for(int i = 0; i < s.size(); i++){
		if(s.get(count).getCatagory().contains(cata)){
			ss.add(s.get(count));
		}
			count++;
		}
		return ss;
	}

	@Override
	public List<Stock> readPriceDesc() {
		
		return repository.findAllByOrderByPriceAsc();
	}

	@Override
	public List<Stock> readTitle(String title) {
		List<Stock> s = repository.findAll();
		List<Stock> ss = new ArrayList<Stock>();
		int count = 0;
		for(int i = 0; i < s.size(); i++){
		if(s.get(count).getTitle().contains(title)){
			ss.add(s.get(count));
		}
			count++;
		}
		return ss;
	}

	@Override
	public List<Stock> readTitleDesc() {
		return repository.findAllByOrderByTitleAsc();
	}

	@Override
	public List<Stock> readManu(String manu) {
		List<Stock> s = repository.findAll();
		List<Stock> ss = new ArrayList<Stock>();
		int count = 0;
		for(int i = 0; i < s.size(); i++){
		if(s.get(count).getManufacturer().contains(manu)){
			ss.add(s.get(count));
		}
			count++;
		}
		return ss;
	}

	@Override
	public List<Stock> readManuDesc() {
		return repository.findAllByOrderByManufacturerAsc();
	}

	@Override
	public List<Stock> readAll() {
		List<Stock> s = repository.findAll();
		return s;
	}

	@Override
	public Stock readOne(Long id) {
		Stock s = repository.findOne(id);
		return s;
	}

}
