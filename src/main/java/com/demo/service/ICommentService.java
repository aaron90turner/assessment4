package com.demo.service;

import java.util.List;

import com.demo.domain.Comment;

public interface ICommentService {

	public Comment create(Comment c);
	public List<Comment> allByUser(Long id);
	public List<Comment> allByStock(Long id);
	public Comment findMyComment(Long id , Long id2);
}
