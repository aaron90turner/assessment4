package com.demo.service;

import java.util.List;











import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.demo.domain.User;
import com.demo.repository.jpa.IUserRepository;

@Service
@Transactional
public class UserService implements IUserService{

	@Autowired
	private IUserRepository repository;
	


	public User create(User user) {
		return repository.save(user);
	}


	public User read(Long id) {
		return repository.findOne(id);
	}


	public List<User> readAll() {
		return repository.findAll();
	}


	public User update(User user) {
		User existingUser = repository.findOne(user.getId());

		// Assign new values
		existingUser.setName(user.getName());
		existingUser.setUsername(user.getUsername());
		existingUser.setEmail(user.getEmail());
		existingUser.setPassword(user.getPassword());

		return repository.save(existingUser);
	}


	public User delete(Long id) {
		repository.delete(id);

		return repository.findOne(id);
	}


	@Override
	public User login(String username, String password) {
		 User user = repository.findByUsernameAndPassword(username, password);
		System.out.println("inside method");
		if(user == null){
			
			System.out.println("log failed");
			
		}else{
			
			System.out.println("log worked");
		}
		return user;
		
	}


	
		
		
	}


	


	


	


