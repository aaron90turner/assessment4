package com.demo.service;

import java.util.List;

import com.demo.domain.Cart;
import com.demo.domain.User;

public interface ICartService {
	
	public Cart create(Cart cart);
	
	public List<Cart> readAllInCart(Long id);
	
	public List<Cart> readAllInCartNotBought(Long id);
	
	public Cart delete(Long id);

}
