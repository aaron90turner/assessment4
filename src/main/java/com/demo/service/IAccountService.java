package com.demo.service;

import java.util.List;

import com.demo.domain.Account;

public interface IAccountService {
	
	public Account create(Account a);
	
	public Account getAccount(Long id);
	
	public List<Account> readAll();

}
