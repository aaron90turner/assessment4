package com.demo.service;

import java.util.List;

import com.demo.domain.Stock;
import com.demo.domain.User;

public interface IStockService {
	
	public Stock create(Stock s);
	public Stock update(Stock s);
	public Stock delete(Long id);
	public List<Stock> readCatagory(String cata); 
	public List<Stock> readPriceDesc(); 
	public List<Stock> readTitle(String title); 
	public List<Stock> readTitleDesc(); 
	public List<Stock> readManu(String manu); 
	public List<Stock> readManuDesc(); 
	public List<Stock> readAll(); 
	public Stock readOne(Long id);
	

}
