package com.demo.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.domain.PurchaseHistory;
import com.demo.domain.Stock;

public interface IHistoryRepository extends JpaRepository<PurchaseHistory, Long> {
	
	List<PurchaseHistory> findAllByHistory_id(Long id);

}
