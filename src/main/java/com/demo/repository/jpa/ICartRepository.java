package com.demo.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.domain.Cart;
import com.demo.domain.PurchaseHistory;
import com.demo.domain.User;

public interface ICartRepository extends JpaRepository<Cart, Long> {

	List<Cart> findAllByBuyer_id(Long id);
	List<Cart> findAllByBuyer_idAndBoughtFalse(Long id);
}
