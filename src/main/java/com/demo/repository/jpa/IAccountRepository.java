package com.demo.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.domain.Account;
import com.demo.domain.PurchaseHistory;
import com.demo.domain.Stock;

public interface IAccountRepository extends JpaRepository<Account, Long>{

	Account findByUser_id(Long id);
}
