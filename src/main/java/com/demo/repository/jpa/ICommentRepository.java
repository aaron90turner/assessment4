package com.demo.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.domain.Comment;
import com.demo.domain.PurchaseHistory;
import com.demo.domain.User;

public interface ICommentRepository extends JpaRepository<Comment, Long> {

	List<Comment> findAllByCommentStock_id(Long id);
	
	List<Comment> findAllByCommentUser_id(Long id);
	
	Comment findByCommentStock_idAndCommentUser_id(Long id, Long id2);
}
