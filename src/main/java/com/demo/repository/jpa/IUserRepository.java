package com.demo.repository.jpa;

import java.util.List;

import javax.persistence.NamedQuery;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.demo.domain.User;

public interface IUserRepository extends JpaRepository<User, Long> {
	
	 User findByUsernameAndPassword(String username, String password);
	

}
