package com.demo.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.domain.Stock;

public interface IStockRepository extends JpaRepository<Stock, Long> {
	
	List<Stock> findAllByOrderByPriceAsc();
	//List<Stock> findAllByPrice();
	List<Stock> findAllByOrderByTitleAsc();
	//List<Stock> findAllByTitle();
	List<Stock> findAllByOrderByManufacturerAsc();
	//List<Stock> findAllByManufacturer();

}
