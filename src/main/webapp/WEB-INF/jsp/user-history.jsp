<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>User History</title>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<link href="${pageContext.request.contextPath}/resources/css/style.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.css"
	rel="stylesheet">
<script src="<c:url value="/resources/js/jquery-2.1.1.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.js" />"></script>

</head>
<body>

	<div id="wrap">

		<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">ElectronicStore</a>
      </div>
      <div class="collapse navbar-collapse ">
        <ul class="nav navbar-nav">
          <li class="active"><a href="${contextPath}/admin-mainpage.html">Home</a></li>
          <li><a href="${contextPath}/add-stock.html">Add Stock</a></li>
          <li><a href="${contextPath}/view-stock.html">View Stock</a></li>
          <li><a href="${contextPath}/all-users.html">Users</a></li>
          <li><a href="${contextPath}/all-history.html">History</a></li>

        </ul>
        <ul class="nav navbar-nav navbar-right">
        <li><a href="${contextPath}/logout.html"><span class="glyphicon glyphicon-lock"></span>Logged in as ADMIN</a></li>
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </div>



		<!-- Begin page content -->
		<div class="container">
	    
	    
	    <div class="push"></div>
			<div class="span3 well bg-profile">
			
				
				<center>
					<h4>
						User Purchase History for <%=session.getAttribute("his-name")%>
						
					</h4>
					<c:if test="${not empty history}">
						<table style="width: 100%">
							<tr bgcolor="#dadada">
							<th style="width: 50px">DATE</th>
								<th style="width: 50px">TITLE</th>
								<th style="width: 50px">MANUFACTURER</th>
								<th style="width: 50px">CATAGORY</th>
								<th style="width: 25px">QUANTITY</th>
								<th style="width: 75px">TOTAL PRICE</th>
								
								
								
								
							</tr>
							<c:forEach var="o" items="${history}" varStatus="theCount">
								

									<tr>
										<td><div class="ladder">${o.date}</div></td>
										<td><div class="ladder">${o.history1.title}</div></td>
										<td><div class="ladder">${o.history1.manufacturer}</div></td>
										<td><div class="ladder">${o.history1.catagory}</div></td>
										<td><div class="ladder">${o.quantity}</div></td>
										<td><div class="ladder">&euro;${o.history1.price}</div></td>
										
										
										
										
									</tr>
								
							</c:forEach>
						</table>
						
					</c:if>
					
					<c:if test="${empty history}">
    No Items Have Been Purchased
</c:if>
				</center>

			</div>
	</div>
	
		</div>
</body>
</html>