<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Registration</title>
<link href="${pageContext.request.contextPath}/resources/css/style.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.css"
	rel="stylesheet">
<script src="<c:url value="/resources/js/jquery-2.1.1.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.js" />"></script>

</head>
<body>

	<div id="wrap">

		



		<!-- Begin page content -->
		<div class="container">
			<div class="row text-center pad-top ">
				<div class="col-md-12">
					
				</div>
			</div>
			<div class="row  pad-top">

				<div
					class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
					<div class="panel panel-default">
						<div class="panel-heading">
							<strong> Register Yourself </strong>
						</div>
						<div class="panel-body">
							<form:form action="registered.html" method="post" commandName="userForm">

								<br />
								
								<font color='red'><form:errors path="name" cssClass="error"/></font>
								<div class="form-group input-group">
									<span class="input-group-addon"></span>
									<form:input path="name" class="form-control" placeholder="Your Name" />
								</div>
								<font color='red'><form:errors path="username" cssClass="error"/></font>
								<div class="form-group input-group">
									<span class="input-group-addon"></span>
									<form:input path="username" class="form-control" placeholder="Your UserName" />
								</div>
								<font color='red'><form:errors path="email" cssClass="error"/></font>
								<div class="form-group input-group">
									<span class="input-group-addon"></span>
									<form:input path="email" class="form-control" placeholder="Your Email" />
								</div>
								
								<font color='red'><form:errors path="password" cssClass="error"/></font>
								<div class="form-group input-group">
									<span class="input-group-addon"></span>
									<form:input type="password" path="password" class="form-control" placeholder="Your Password" />
								</div>


								<input type="submit" value="Register" />
								<hr />
                                    Already Registered ?  <a
									href="login.html">Login here</a>
							</form:form>

						</div>

					</div>
				</div>

</div>
			</div>
		</div>
</body>
</html>