<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stock</title>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">
<link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet">
 <script src="<c:url value="/resources/js/jquery-2.1.1.min.js" />"></script>

<script src="<c:url value="/resources/js/bootstrap.js" />"></script>

</head>
<body>

	<div id="wrap">

		<!-- Fixed navbar -->
		<div class="navbar navbar-default navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target=".navbar-collapse">
						<span class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">ElectronicShop</a>
				</div>
				<div class="collapse navbar-collapse ">
					<ul class="nav navbar-nav">
						<li class="active"><a href="${contextPath}/user-mainpage.html">Home</a></li>
						<li><a href="${contextPath}/stock.html">Stock</a></li>
						<li><a href="${contextPath}/my-history.html">My History</a></li>	
					</ul>
					<ul class="nav navbar-nav navbar-right">
					<c:if test="${not empty cart }">
					<c:choose>
					<c:when test="${cart == '0'}">
								<li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span>No Items in cart</a></li>
								</c:when>
					<c:otherwise>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown"><span class="glyphicon glyphicon-shopping-cart"></span>Items in cart = ${cart}<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="checkout.html">Checkout</a></li>
								<li><a href="empty.html">Empty Cart</a></li>
								</ul>
								</li>
							</c:otherwise>
								</c:choose>
								</c:if>
							<c:if test="${empty cart }">
							<li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span>No Items in cart</a></li>
							</c:if>
							<c:if test="${sessionScope.name != null}">
							<li><a href="${contextPath}/logout.html"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>
							</c:if>
					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
		</div>


<!-- Begin page content -->
		<div class="container">
	    
	    <div class="push"></div>
	    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">
            
        </h3>
      </div>
      <div class="panel-body">
        
        <div class="row">
           <div class="col-sm-12">
               <div class="form-inline">             

                    <button class="btn btn-default" id="ss-sort">Sort By Title</button>  
                    <button class="btn btn-default" id="ss-sort2">Sort By Catagory</button> 
                    <button class="btn btn-default" id="ss-sort3">Sort By Price</button> 
                    <button class="btn btn-default" id="ss-all">All</button>  
                    <input type="text" class="form-control" id="ss-name-filter" placeholder="Filter by Title">
                    <input type="text" class="form-control" id="ss-description-filter" placeholder="Filter by Catagory">
                    <input type="text" class="form-control" id="ss-manu-filter" placeholder="Filter by Manufacturer">
                    
                </div>
           </div>
        </div>
        
        <hr>

        <div class="ss-box">
         <c:if test="${not empty stockList}">
         <c:forEach var="o" items="${stockList}" varStatus="theCount">
          <div class="col-sm-6 col-md-4"
           data-description="${o.catagory}"
           data-name="${o.title}" data-manu="${o.manufacturer}" data-price="${o.price}">
            <div class="thumbnail">
              <div class="caption">
                <h3>${o.title}</h3>
                <p><a href="stock-info/${o.id}.html"><img src="${pageContext.request.contextPath}/resources/img/${o.image} " width="70" height="70" /></a></p>
                <p><b>MANUFACTURER:</b> ${o.manufacturer}</p>
                <p><b>CATAGORY:</b> ${o.catagory}</p>
                <p><b>QUANTITY:</b> ${o.quantity}</p>
                <p><b>PRICE:</b>${o.price}&euro;</p>
              </div>
            </div>
          </div>
          </c:forEach>
          </c:if>

         
           
        </div>

      </div>
    </div> 
	
	
		</div>
		</div>
		
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="<c:url value="/resources/js/jquery-2.1.1.min.js" />"></script>

         <script src="<c:url value="/resources/js/simpleSort.js" />"></script>



<script>
$(document).ready( function() {

    var test = new simpleSort('.ss-box', 'div');

    $('#ss-sort').on('click', function() {

        // toggle sort
        if(test.order === 'desc') {
            test.sort('data-name', 'asc');
        } else {
            test.sort('data-name', 'desc');
        }

    });
    
    $('#ss-sort2').on('click', function() {

        // toggle sort
        if(test.order === 'desc') {
            test.sort('data-description', 'asc');
        } else {
            test.sort('data-description', 'desc');
        }

    });
    
    $('#ss-sort3').on('click', function() {

        // toggle sort
        if(test.order === 'desc') {
            test.sort('data-price', 'asc');
        } else {
            test.sort('data-price', 'desc');
        }

    });

    
    
    $('#ss-all').on('click', function() {
        test.all();
    });

    $('#ss-name-filter').on('propertychange change keyup paste input mouseup', function() {
        test.filter('data-name', $(this).val());
    });
    
    $('#ss-description-filter').on('propertychange change keyup paste input mouseup', function() {
        test.filter('data-description', $(this).val());
    }); 
    
    $('#ss-manu-filter').on('propertychange change keyup paste input mouseup', function() {
        test.filter('data-manu', $(this).val());
    });  
});    
</script>
</body>
</html>