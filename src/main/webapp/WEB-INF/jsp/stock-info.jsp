<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Stock Info</title>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<link href="${pageContext.request.contextPath}/resources/css/style.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.css"
	rel="stylesheet">
	<script src="<c:url value="/resources/js/jquery-2.1.1.min.js" />"></script>
	
<script src="<c:url value="/resources/js/bootstrap.js" />"></script>
</head>
<body>

	<div id="wrap">

		<!-- Fixed navbar -->
		<div class="navbar navbar-default navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target=".navbar-collapse">
						<span class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">ElectronicShop</a>
				</div>
				<div class="collapse navbar-collapse ">
					<ul class="nav navbar-nav">
						<li class="active"><a href="${contextPath}/user-mainpage.html">Home</a></li>
						<li><a href="${contextPath}/stock.html">Stock</a></li>
						<li><a href="${contextPath}/my-history.html">My History</a></li>	
					</ul>
					<ul class="nav navbar-nav navbar-right">
					<c:if test="${not empty cart }">
					<c:choose>
					<c:when test="${cart == '0'}">
								<li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span>No Items in cart</a></li>
								</c:when>
					<c:otherwise>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown"><span class="glyphicon glyphicon-shopping-cart"></span>Items in cart = ${cart}<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="checkout.html">Checkout</a></li>
								<li><a href="empty.html">Empty Cart</a></li>
								</ul>
								</li>
							</c:otherwise>
								</c:choose>
								</c:if>
							<c:if test="${empty cart }">
							<li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span>No Items in cart</a></li>
							</c:if>
							<c:if test="${sessionScope.name != null}">
							<li><a href="${contextPath}/logout.html"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>
							</c:if>
					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
		</div>



		<div class="container">
        <div class="row text-center pad-top ">
            <div class="col-md-12">
            </div>
        </div>
         <div class="row  pad-top">
               
                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                        <strong>Stock Info</strong>  
                            </div>
                            <div class="panel-body">
                                
                                    <c:if test="${not empty stockItem}">
                                     <center></center>
                                     
                                     <center><img src="${pageContext.request.contextPath}/resources/img/${stockItem.image} " width="100" height="100" /></center>
                                    <hr />
                                     <center>TITLE: ${stockItem.title}</center>
                                     <center>MANUFACTURER: ${stockItem.manufacturer}</center>
                                     <center>CATAGORY: ${stockItem.catagory}</center>
                                     <center>QUANTITY: ${stockItem.quantity}</center>
                                   <hr />
                                  <center>PRICE: &euro;${stockItem.price}</center>
                                  <hr />
                                  <a href="#aboutModal" data-toggle="modal" data-target="#myModal"><center>click to read reviews</center></a>
                                    <hr />
                                    <c:if test="${stockItem.quantity == '0'}">
                                    <font color='red'>This Item is Out of stock</font>
                                    </c:if>
                                      <c:if test="${stockItem.quantity >= '1'}">
                                  <form:form action="added-to-cart.html" method="post" commandName="cartForm">
                                  <div class="form-group input-group">
												<span class="input-group-addon"></span>
												<form:select path="quantity"
													class="form-control">
													<form:options items="${quantityList}" />
												</form:select>
											</div>
                                  <input type="submit" value="Add To Cart" />
                                  </form:form>
                                  </c:if>
                                  
                                  <hr />
                                    Not Interested?  <a
									href="${contextPath}/stock.html">Return to Stock</a>
                                  </c:if>
                                    
                            </div>
                           
                        </div>
                    </div>
                    
                    
                    
                    <!-- Modal -->
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
				aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true"></button>
							<h4 class="modal-title" id="myModalLabel"> REVIEWS</h4>
						</div>
						<c:if test="${not empty comment}">
						<c:forEach var="o" items="${comment}" varStatus="theCount">
						<div class="modal-body bg-profile ">
							<center>
							  <hr />
                                     <center>USER: ${o.commentUser.username}</center>
                                  
                                     <center>COMMENT: ${o.comment}</center>
                                <c:if test="${o.rating == '1'}">
                                   <center><span class="glyphicon glyphicon-star"><span class="glyphicon glyphicon-star-empty"></span><span class="glyphicon glyphicon-star-empty"></span><span class="glyphicon glyphicon-star-empty"></span><span class="glyphicon glyphicon-star-empty"></span></center>
                                   </c:if>
                                   <c:if test="${o.rating == '2'}">
                                   <center><span class="glyphicon glyphicon-star"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star-empty"></span><span class="glyphicon glyphicon-star-empty"></span><span class="glyphicon glyphicon-star-empty"></span></center>
                                   </c:if>
                                   <c:if test="${o.rating == '3'}">
                                   <center><span class="glyphicon glyphicon-star"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star-empty"></span><span class="glyphicon glyphicon-star-empty"></span></center>
                                   </c:if>
                                   <c:if test="${o.rating == '4'}">
                                   <center><span class="glyphicon glyphicon-star"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star-empty"></span></center>
                                   </c:if>
                                   <c:if test="${o.rating == '5'}">
                                   <center><span class="glyphicon glyphicon-star"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span></center>
                                   </c:if>
                                  <hr />
                                     
                                 
							</center>
						</div>
						</c:forEach>
						</c:if>
						<c:if test="${empty comment}">
						<div class="modal-body bg-profile ">
						<center>NO REVIEWS FOR THIS PRODUCT YET</center>
						</div>
						</c:if>
						<div class="modal-footer">
							<center>
								<button type="button" class="btn btn-default"
									data-dismiss="modal">Close</button>
							</center>
						</div>
					</div>
				</div>
			</div>
                
                </div>
        </div>
    </div>
</body>
</html>