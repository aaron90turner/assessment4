<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Registration</title>
<link href="${pageContext.request.contextPath}/resources/css/style.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.css"
	rel="stylesheet">
	<script src="<c:url value="/resources/js/jquery-2.1.1.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.js" />"></script>
</head>
<body>

	<div id="wrap">

		


		<!-- Begin page content -->
		<div class="container">
			<div class="row text-center pad-top ">
				<div class="col-md-12">
					
				</div>
			</div>
			<div class="row  pad-top">

				<div
					class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
					<div class="panel panel-default">
						<div class="panel-heading">
							<strong> Login Yourself </strong>
						</div>
						<div class="panel-body">
							<form:form action="login-complete.html" method="post" commandName="userForm">
								<br />

								<div class="form-group input-group">
									<span class="input-group-addon"></span> <form:input path="username" id="uname"  class="form-control" placeholder="Your Username"  />
								</div>

								<div class="form-group input-group">
									<span class="input-group-addon"></span> <form:input type="password" path="password" class="form-control" placeholder="Your Password"  />
								</div>


								<input type="submit" value="Login" />
								<hr />
								Not Registered ? <a href="register.html">Register here</a>
							</form:form>
						</div>

					</div>
				</div>


			</div>
		</div>








	</div>









</body>
</html>