<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>admin-mainpage</title>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" >
<link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet" >
<script src="<c:url value="/resources/js/jquery-2.1.1.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.js" />"></script>

</head>
<body>
     <div id="wrap">
  
  <!-- Fixed navbar -->
  <div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">ElectronicStore</a>
      </div>
      <div class="collapse navbar-collapse ">
        <ul class="nav navbar-nav">
          <li class="active"><a href="${contextPath}/admin-mainpage.html">Home</a></li>
          <li><a href="${contextPath}/add-stock.html">Add Stock</a></li>
          <li><a href="${contextPath}/view-stock.html">View Stock</a></li>
          <li><a href="${contextPath}/all-users.html">Users</a></li>
          <li><a href="${contextPath}/all-history.html">History</a></li>

        </ul>
        <ul class="nav navbar-nav navbar-right">
        <li><a href="${contextPath}/logout.html"><span class="glyphicon glyphicon-lock"></span>Logged in as ADMIN</a></li>
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </div>


  
  <!-- Begin page content -->
  <div class="center-container">
     <div class="center-row2">
      <div class="col-xs-6 bg-one text-center"><h2><a href="add-stock.html">ADD STOCK</a></h2></div>
      <div class="col-xs-6 bg-four text-center"><h2><a href="view-stock.html">VIEW STOCK</a></h2></div>
      
    </div>
    <div class="center-row2">
      <div class="col-xs-6 bg-three text-center"><h2><a href="all-users.html">VIEW USERS</a></h2></div>
      <div class="col-xs-6 bg-two text-center"><h2><a href="all-history.html">VIEW HISTORY</a></h2></div>
      
    </div>
  </div>
</div>

<div id="footer">
  <div class="container">
    <p>&nbsp;</p>
    <p class="lead">AARON TURNER</p>
  </div>
</div>


</body>
</html>