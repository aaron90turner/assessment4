<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>My History</title>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<link href="${pageContext.request.contextPath}/resources/css/style.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.css"
	rel="stylesheet">
<script src="<c:url value="/resources/js/jquery-2.1.1.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.js" />"></script>

</head>
<body>

	<div id="wrap">

		<!-- Fixed navbar -->
		<div class="navbar navbar-default navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target=".navbar-collapse">
						<span class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">ElectronicShop</a>
				</div>
				<div class="collapse navbar-collapse ">
					<ul class="nav navbar-nav">
						<li class="active"><a href="${contextPath}/user-mainpage.html">Home</a></li>
						<li><a href="${contextPath}/stock.html">Stock</a></li>
						<li><a href="${contextPath}/my-history.html">My History</a></li>	
					</ul>
					<ul class="nav navbar-nav navbar-right">
					<c:if test="${not empty cart }">
					<c:choose>
					<c:when test="${cart == '0'}">
								<li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span>No Items in cart</a></li>
								</c:when>
					<c:otherwise>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown"><span class="glyphicon glyphicon-shopping-cart"></span>Items in cart = ${cart}<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="checkout.html">Checkout</a></li>
								<li><a href="empty.html">Empty Cart</a></li>
								</ul>
								</li>
							</c:otherwise>
								</c:choose>
								</c:if>
							<c:if test="${empty cart }">
							<li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span>No Items in cart</a></li>
							</c:if>
							<c:if test="${sessionScope.name != null}">
							<li><a href="${contextPath}/logout.html"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>
							</c:if>
					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
		</div>



		<!-- Begin page content -->
		<div class="container">
	    
	    
	    <div class="push"></div>
			<div class="span3 well bg-profile">
			
				
				<center>
					<h4>
						Purchase History
						
					</h4>
					<c:if test="${not empty history}">
						<table style="width: 100%">
							<tr bgcolor="#dadada">
							<th style="width: 50px">DATE</th>
								<th style="width: 50px">TITLE</th>
								<th style="width: 50px">MANUFACTURER</th>
								<th style="width: 50px">CATAGORY</th>
								<th style="width: 25px">QUANTITY</th>
								<th style="width: 75px">TOTAL PRICE</th>
								<th style="width: 50px">REVIEW</th>
								
								
								
							</tr>
							<c:forEach var="o" items="${history}" varStatus="theCount">
								

									<tr>
										<td><div class="ladder">${o.date}</div></td>
										<td><div class="ladder">${o.history1.title}</div></td>
										<td><div class="ladder">${o.history1.manufacturer}</div></td>
										<td><div class="ladder">${o.history1.catagory}</div></td>
										<td><div class="ladder">${o.quantity}</div></td>
										<td><div class="ladder">&euro;${o.history1.price}</div></td>
										<c:if test="${o.reviewed == 'false'}">
										<td><div class="ladder"><a href="write-review/${o.id}.html">Write Review</a></div></td>
										</c:if>
										<c:if test="${o.reviewed == 'true'}">
								       <td><div class="ladder"><a href="my-review/${o.history1.id}.html">View Review</a></div></td>
										
										</c:if>
										
									</tr>
								
							</c:forEach>
						</table>
						
					</c:if>
					
					<c:if test="${empty history}">
    No Items Have Been Purchased
</c:if>
				</center>

			</div>
	
	</div>
		</div>
</body>
</html>