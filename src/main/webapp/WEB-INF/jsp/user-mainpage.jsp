<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>user-mainpage</title>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" >
<link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet" >
<script src="<c:url value="/resources/js/jquery-2.1.1.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.js" />"></script>

</head>
<body>
     <div id="wrap">
  
  <!-- Fixed navbar -->
		<div class="navbar navbar-default navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target=".navbar-collapse">
						<span class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">ElectronicShop</a>
				</div>
				<div class="collapse navbar-collapse ">
					<ul class="nav navbar-nav">
						<li class="active"><a href="${contextPath}/user-mainpage.html">Home</a></li>
						<li><a href="${contextPath}/stock.html">Stock</a></li>
						<li><a href="${contextPath}/my-history.html">My History</a></li>	
					</ul>
					<ul class="nav navbar-nav navbar-right">
					<c:if test="${not empty cart }">
					<c:choose>
					<c:when test="${cart == '0'}">
								<li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span>No Items in cart</a></li>
								</c:when>
					<c:otherwise>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown"><span class="glyphicon glyphicon-shopping-cart"></span>Items in cart = ${cart}<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="checkout.html">Checkout</a></li>
								<li><a href="empty.html">Empty Cart</a></li>
								</ul>
								</li>
							</c:otherwise>
								</c:choose>
								</c:if>
							<c:if test="${empty cart }">
							<li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span>No Items in cart</a></li>
							</c:if>
							<c:if test="${sessionScope.name != null}">
							<li><a href="${contextPath}/logout.html"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>
							</c:if>
					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
		</div>


  
  <!-- Begin page content -->
  <div class="center-container">
     <div class="center-row2">
      <div class="col-xs-6 bg-one text-center"><h2><a href="my-history.html">PURCHASE HISTORY</a></h2></div>
      <div class="col-xs-6 bg-two text-center"><h2><a href="stock.html">VIEW STOCK</a></h2></div>
    </div>
    <div class="center-row2">
      <div class="col-xs-6 bg-three text-center"><h2><a href="checkout.html">SHOPPING CART</a></h2></div>
      <div class="col-xs-6 bg-four text-center"><h2><a href="logout.html">LOGOUT</a></h2></div>
    </div>
  </div>
</div>

<div id="footer">
  <div class="container">
    <p>&nbsp;</p>
    <p class="lead">AARON TURNER</p>
  </div>
</div>


</body>
</html>