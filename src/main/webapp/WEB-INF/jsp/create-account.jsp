<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Create-account</title>
<link href="${pageContext.request.contextPath}/resources/css/style.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.css"
	rel="stylesheet">
<script src="<c:url value="/resources/js/jquery-2.1.1.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.js" />"></script>

</head>
<body>

	<div id="wrap">

		



		<!-- Begin page content -->
		<div class="container">
			<div class="row text-center pad-top ">
				<div class="col-md-12">
					
				</div>
			</div>
			<div class="row  pad-top">

				<div
					class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
					<div class="panel panel-default">
						<div class="panel-heading">
							<strong> Create Account </strong>
						</div>
						<div class="panel-body">
							<form:form action="account-created.html" method="post" commandName="accountForm">

								<br />
								
								<font color='red'><form:errors path="shippingAddress" cssClass="error"/></font>
								<div class="form-group input-group">
									<span class="input-group-addon"></span>
									<form:input path="shippingAddress" class="form-control" placeholder="Address" />
								</div>
								<font color='red'><form:errors path="country" cssClass="error"/></font>
								<div class="form-group input-group">
									<span class="input-group-addon"></span>
									<form:input path="country" class="form-control" placeholder="Country" />
								</div>
								<font color='red'><form:errors path="city" cssClass="error"/></font>
								<div class="form-group input-group">
									<span class="input-group-addon"></span>
									<form:input path="city" class="form-control" placeholder="City" />
								</div>
								
								<font color='red'><form:errors path="paymentMethod" cssClass="error"/></font>
								<div class="form-group input-group">
												<span class="input-group-addon"></span>
												<form:select path="paymentMethod"
													class="form-control">
													<form:options items="${cardList}" />
												</form:select>
											</div>


								<input type="submit" value="Register" />
								<hr />
                                    Already Registered ?  <a
									href="login.html">Login here</a>
							</form:form>

						</div>

					</div>
				</div>

</div>
			</div>
		</div>
</body>
</html>