<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add Stock</title>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<link href="${pageContext.request.contextPath}/resources/css/style.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.css"
	rel="stylesheet">
<script src="<c:url value="/resources/js/jquery-2.1.1.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.js" />"></script>

</head>
<body>

	<div id="wrap">

		<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">ElectronicStore</a>
      </div>
      <div class="collapse navbar-collapse ">
        <ul class="nav navbar-nav">
          <li class="active"><a href="${contextPath}/admin-mainpage.html">Home</a></li>
          <li><a href="${contextPath}/add-stock.html">Add Stock</a></li>
          <li><a href="${contextPath}/view-stock.html">View Stock</a></li>
          <li><a href="${contextPath}/all-users.html">Users</a></li>
          <li><a href="${contextPath}/all-history.html">History</a></li>

        </ul>
        <ul class="nav navbar-nav navbar-right">
        <li><a href="${contextPath}/logout.html"><span class="glyphicon glyphicon-lock"></span>Logged in as ADMIN</a></li>
        </ul>
      </div><!--/.nav-collapse -->
    </div>
  </div>



		<!-- Begin page content -->
		<div class="container">
			<div class="row text-center pad-top ">
				<div class="col-md-12">
					
				</div>
			</div>
			<div class="row  pad-top">

				<div
					class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
					<div class="panel panel-default">
						<div class="panel-heading">
							<strong>Add Stock</strong>
						</div>
						<div class="panel-body">
							<form:form action="stock-added.html" enctype="multipart/form-data"  method="post" commandName="stockForm">

								<br />
								
								<font color='red'><form:errors path="title" cssClass="error"/></font>
								<div class="form-group input-group">
									<span class="input-group-addon"></span>
									<form:input path="title" class="form-control" placeholder="Stock Title" />
								</div>
								<font color='red'><form:errors path="manufacturer" cssClass="error"/></font>
								<div class="form-group input-group">
									<span class="input-group-addon"></span>
									<form:input path="manufacturer" class="form-control" placeholder="Stock Manufacturer" />
								</div>
								<font color='red'><form:errors path="catagory" cssClass="error"/></font>
								<div class="form-group input-group">
									<span class="input-group-addon"></span>
									<form:input path="catagory" class="form-control" placeholder="Stock Catagory" />
								</div>
								
							
								<div class="form-group input-group">
									<span class="input-group-addon"></span>
									<form:input type="number" path="quantity" class="form-control" placeholder="Stock Quantity" />
								</div>
								
								
								<div class="form-group input-group">
									<span class="input-group-addon"></span>
									<form:input type="number" path="price" class="form-control" placeholder="Stock Price" />
								</div>
								
								<div class="form-group input-group">
									<span class="input-group-addon"></span> <form:input type="file" path="file" />
								</div>


								<input type="submit" value="Submit" />
								<hr />
                                   
							</form:form>

						</div>

					</div>
				</div>
</div>

			</div>
		</div>
</body>
</html>