<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>My Review</title>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<link href="${pageContext.request.contextPath}/resources/css/style.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/css/bootstrap.css"
	rel="stylesheet">
	<script src="<c:url value="/resources/js/jquery-2.1.1.min.js" />"></script>
	
<script src="<c:url value="/resources/js/bootstrap.js" />"></script>
</head>
<body>

	<div id="wrap">

		<!-- Fixed navbar -->
		<div class="navbar navbar-default navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target=".navbar-collapse">
						<span class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">ElectronicShop</a>
				</div>
				<div class="collapse navbar-collapse ">
					<ul class="nav navbar-nav">
						<li class="active"><a href="${contextPath}/user-mainpage.html">Home</a></li>
						<li><a href="${contextPath}/stock.html">Stock</a></li>
						<li><a href="${contextPath}/my-history.html">My History</a></li>	
					</ul>
					<ul class="nav navbar-nav navbar-right">
					<c:if test="${not empty cart }">
					<c:choose>
					<c:when test="${cart == '0'}">
								<li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span>No Items in cart</a></li>
								</c:when>
					<c:otherwise>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown"><span class="glyphicon glyphicon-shopping-cart"></span>Items in cart = ${cart}<b class="caret"></b></a>
							<ul class="dropdown-menu">
								<li><a href="checkout.html">Checkout</a></li>
								<li><a href="empty.html">Empty Cart</a></li>
								</ul>
								</li>
							</c:otherwise>
								</c:choose>
								</c:if>
							<c:if test="${empty cart }">
							<li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span>No Items in cart</a></li>
							</c:if>
							<c:if test="${sessionScope.name != null}">
							<li><a href="${contextPath}/logout.html"><span class="glyphicon glyphicon-lock"></span>Logout</a></li>
							</c:if>
					</ul>
				</div>
				<!--/.nav-collapse -->
			</div>
		</div>



		<div class="container">
        <div class="row text-center pad-top ">
            <div class="col-md-12">
            </div>
        </div>
         <div class="row  pad-top">
               
                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                        <strong>My Review</strong>  
                            </div>
                            <div class="panel-body">
                                
                                    <c:if test="${not empty comment}">
                                     <center></center>
                                     
                                     <center><img src="${pageContext.request.contextPath}/resources/img/${comment.commentStock.image} " width="100" height="100" /></center>
                                     <center>TITLE: ${comment.commentStock.title}</center>
                                     <center>MANUFACTURER: ${comment.commentStock.manufacturer}</center>
                                     <center>CATAGORY: ${comment.commentStock.catagory}</center>
                                     <center>PRICE: &euro;${comment.commentStock.price}</center>
                                  <hr />
                                     <center>COMMENT: ${comment.comment}</center>
                                  <hr />
                                     <c:if test="${comment.rating == '1'}">
                                   <center>RATING: <span class="glyphicon glyphicon-star"><span class="glyphicon glyphicon-star-empty"></span><span class="glyphicon glyphicon-star-empty"></span><span class="glyphicon glyphicon-star-empty"></span><span class="glyphicon glyphicon-star-empty"></span></center>
                                   </c:if>
                                   <c:if test="${comment.rating == '2'}">
                                   <center>RATING: <span class="glyphicon glyphicon-star"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star-empty"></span><span class="glyphicon glyphicon-star-empty"></span><span class="glyphicon glyphicon-star-empty"></span></center>
                                   </c:if>
                                   <c:if test="${comment.rating == '3'}">
                                   <center>RATING: <span class="glyphicon glyphicon-star"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star-empty"></span><span class="glyphicon glyphicon-star-empty"></span></center>
                                   </c:if>
                                   <c:if test="${comment.rating == '4'}">
                                   <center>RATING: <span class="glyphicon glyphicon-star"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star-empty"></span></center>
                                   </c:if>
                                   <c:if test="${comment.rating == '5'}">
                                   <center>RATING: <span class="glyphicon glyphicon-star"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span></center>
                                   </c:if>
                                  <hr />
                                     <a href="${contextPath}/my-history.html">Return to History</a>
                                  </c:if>
                                    
                            </div>
                           
                        </div>
                    </div>
                </div>
                
        </div>
    </div>
</body>
</html>