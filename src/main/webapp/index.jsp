<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ElectronicStore</title>
<link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" >
<link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet" >
<script src="<c:url value="/resources/js/jquery-2.1.1.min.js" />"></script>
<script src="<c:url value="/resources/js/bootstrap.js" />"></script>

</head>
<body>
     <div id="wrap">
  
 


  
  <!-- Begin page content -->
  <div class="center-container">
    <div class="center-row">
      <div class="col-xs-12 bg-one text-center"><h2><a href="login.html">LOGIN</a></h2></div>
      
    </div>
    <div class="center-row">
      <div class="col-xs-12 bg-three text-center"><h2><a href="register.html">REGISTER</a></h2></div>
      
    </div>
  </div>
</div>

<div id="footer">
  <div class="container">
    <p>&nbsp;</p>
    <p class="lead">AARON TURNER</p>
  </div>
</div>


</body>
</html>